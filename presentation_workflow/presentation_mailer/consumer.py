import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='approve')
        channel.queue_declare(queue='reject')

        def process_approval(ch, method, properties, body):
            p = json.loads(body)
            send_mail(
            "Your presentation has been accepted",
            f"{p['presenter_name']}, we're happy to tell you that your presentation {p['title']} has been accepted",
            "admin@conference.go",
            [f'{p["presenter_email"]}'],
            fail_silently=False,
        )

        def process_rejection(ch, method, properties, body):
            p = json.loads(body)
            send_mail(
            "Your presentation has been rejected",
            f"{p['presenter_name']}, we're sorry to tell you that your presentation {p['title']} has been rejected",
            "admin@conference.go",
            [f'{p["presenter_email"]}'],
            fail_silently=False,
        )
        channel.basic_consume(
            queue='approve',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue='reject',
            on_message_callback=process_rejection ,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
